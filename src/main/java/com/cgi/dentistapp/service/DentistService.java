package com.cgi.dentistapp.service;

import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.repository.DentistDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
@AllArgsConstructor
public class DentistService {

  private final DentistDao dentistDao;

  public List<Dentist> getAllDentists() {
    List<Dentist> dentists = new ArrayList<>();
    dentistDao.findAll().forEach(dentists::add);
    return dentists;
  }

  public Map<Long, Dentist> getDentistsById() {
    Map<Long, Dentist> dentistsById = new HashMap<>();
    dentistDao.findAll().forEach(dentist -> dentistsById.put(dentist.getId(), dentist));
    return dentistsById;
  }
}
