package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.service.DentistVisitService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
public class HasValidDate implements ValidationRule {

  public static final int DATE_MAX_ALLOWED_MONTHS_IN_FUTURE = 3;
  private final List<DayOfWeek> INVALID_DAYS = Arrays.asList(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
  private final Logger LOGGER = LoggerFactory.getLogger(HasValidDate.class);
  private final DentistVisitService visitService;

  @Override
  public void apply(DentistVisitDto dto, ValidationResult result) {
    LOGGER.debug(String.format("Validating visit date: %s",  dto));
    LocalDate date = dto.getDate();

    if (date == null) {
      return;
    }

    if (date.isBefore(LocalDate.now())) {
      result.addFieldError("date", "DentistVisitDto.error.dateInThePast");
    }

    if (date.isAfter(LocalDate.now().plusMonths(DATE_MAX_ALLOWED_MONTHS_IN_FUTURE))) {
      result.addFieldError("date", "DentistVisitDto.error.dateTooFarInTheFuture");
    }

    if (INVALID_DAYS.contains(date.getDayOfWeek())) {
      result.addFieldError("date", "DentistVisitDto.error.dateDuringWeekend");
    }

    if (dto.getDentistId() == null) {
      return;
    }

    List<LocalTime> availableTimes = visitService.findAvailableTimes(dto.getDentistId(), dto.getDate());
    if (availableTimes.isEmpty()) {
      result.addFieldError("date", "DentistVisitDto.error.noAvailableTimes");
    }
  }
}
