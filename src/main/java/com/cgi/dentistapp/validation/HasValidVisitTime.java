package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static com.cgi.dentistapp.service.DentistVisitService.HOURS_FROM_NOW_TO_ADD_NEW_VISIT;

@AllArgsConstructor
public class HasValidVisitTime implements ValidationRule {

  private final Logger LOGGER = LoggerFactory.getLogger(HasValidVisitTime.class);
  private final DentistVisitService visitService;
  private final DentistService dentistService;

  @Override
  public void apply(DentistVisitDto dto, ValidationResult result) {
    LOGGER.debug(String.format("Validating registration date and time: %s", dto));
    LocalDate date = dto.getDate();
    LocalTime time = dto.getTime();

    if (time == null) {
      result.addFieldError("time", "NotNull.dentistVisitDto.time");
      return;
    }

    if (date == null) {
      return;
    }

    if (LocalDate.now().isEqual(date)) {
      if (time.isBefore(LocalTime.now())) {
        result.addFieldError("time", "DentistVisitDto.error.timeInThePast");
      } else if (LocalTime.now().plusHours(HOURS_FROM_NOW_TO_ADD_NEW_VISIT).isAfter(time)) {
        result.addFieldError("time", "DentistVisitDto.error.todaysRegistrationTooSoon");
      }
      return;
    }

    if (dto.getId() != null) {
      DentistVisitDto savedVisit = visitService.findVisit(dto.getId());
      if (savedVisit != null && hasSameDateAndTimeAsSavedVisit(dto, savedVisit)) {
        return;
      }
    }

    List<LocalTime> availableTimes = visitService.findAvailableTimes(dto.getDentistId(), dto.getDate());
    if (availableTimes.isEmpty()) {
      result.addFieldError("time", "DentistVisitDto.error.noAvailableTimes");
    }

    if (!availableTimes.contains(dto.getTime())) {
      result.addGlobalError("time.error", createErrors(dto));
    }
  }

  private boolean hasSameDateAndTimeAsSavedVisit(DentistVisitDto updatedVisit, DentistVisitDto savedVisit) {
    return updatedVisit.getDate().isEqual(savedVisit.getDate())
        && updatedVisit.getTime().equals(savedVisit.getTime());
  }

  private Object[] createErrors(DentistVisitDto dto) {
    Map<Long, Dentist> dentists = dentistService.getDentistsById();
    return new Object[] {
        dentists.get(dto.getDentistId()).getFullName(),
        dto.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
        dto.getTime()
    };
  }
}
