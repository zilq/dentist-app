package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.service.DentistService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AllArgsConstructor
public class HasValidDentist implements ValidationRule {

  private final Logger LOGGER = LoggerFactory.getLogger(HasValidDentist.class);
  private final DentistService dentistService;

  @Override
  public void apply(DentistVisitDto dto, ValidationResult result) {
    LOGGER.debug(String.format("Validating dentist: %s", dto));
    if (dto.getDentistId() == null) {
      return;
    }

    if (!dentistService.getDentistsById().containsKey(dto.getDentistId())) {
      result.addFieldError("dentistId", "DentistVisitDto.error.invalidDentistId");
    }
  }
}
