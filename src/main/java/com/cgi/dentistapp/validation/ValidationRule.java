package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;

public interface ValidationRule {

  void apply(DentistVisitDto dto, ValidationResult result);

}
