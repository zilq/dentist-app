package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.entity.Dentist;
import org.springframework.data.repository.CrudRepository;

public interface DentistDao extends CrudRepository<Dentist, Long> {
}
