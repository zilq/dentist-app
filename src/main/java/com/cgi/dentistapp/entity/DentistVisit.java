package com.cgi.dentistapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "dentist_visit")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DentistVisit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long dentistId;
    private Date visitDate;

    public DentistVisit(Long dentistId, Date visitDate) {
        this.dentistId = dentistId;
        this.visitDate = visitDate;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return String.format("DentistVisit: [id=%d, dentistId=%d, visitDate=%s]",
            id, dentistId, formatter.format(visitDate));
    }
}
