package com.cgi.dentistapp.validation;

import com.cgi.dentistapp.dto.DentistVisitDto;
import com.cgi.dentistapp.service.DentistVisitService;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;

import static com.cgi.dentistapp.validation.HasValidDate.DATE_MAX_ALLOWED_MONTHS_IN_FUTURE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HasValidDateTest {

  private DentistVisitService mockedDentistVisitService;
  private ValidationRule validationRule;

  @Before
  public void setUp() {
    mockedDentistVisitService = mock(DentistVisitService.class);
    validationRule = new HasValidDate(mockedDentistVisitService);
  }

  @Test
  public void validDateHasNoErrors() {
    LocalDate nextMonday = getNextNearestDayOfWeek(LocalDate.now(), DayOfWeek.MONDAY);
    DentistVisitDto dto = createDto(nextMonday);
    mockAvailableTimes(dto.getDentistId(), dto.getDate(), dto.getTime());
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertFalse(validationResult.hasErrors());
  }

  @Test
  public void dateInThePastHasErrors() {
    DentistVisitDto dto = createDto(LocalDate.now().minusDays(1L));
    mockAvailableTimes(dto.getDentistId(), dto.getDate(), dto.getTime());
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void dateInTheWeekendHasErrors() {
    DentistVisitDto dto = createDto(getNextNearestDayOfWeek(LocalDate.now(), DayOfWeek.SATURDAY));
    mockAvailableTimes(dto.getDentistId(), dto.getDate(), dto.getTime());
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void dateTooFarInTheFutureHasErrors() {
    LocalDate dateTooFarInTheFuture = LocalDate.now().plusMonths(DATE_MAX_ALLOWED_MONTHS_IN_FUTURE + 1L);
    DentistVisitDto dto = createDto(getNextNearestDayOfWeek(dateTooFarInTheFuture, DayOfWeek.MONDAY));
    mockAvailableTimes(dto.getDentistId(), dto.getDate(), dto.getTime());
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  @Test
  public void registrationWithNoAvailableTimesHasErrors() {
    LocalDate date = LocalDate.now();
    DentistVisitDto dto = createDto(getNextNearestDayOfWeek(date, DayOfWeek.MONDAY));
    mockAvailableTimes(dto.getDentistId(), dto.getDate(), null);
    ValidationResult validationResult = new ValidationResult();
    validationRule.apply(dto, validationResult);
    assertTrue(validationResult.hasErrors());
  }

  private DentistVisitDto createDto(LocalDate date) {
    return DentistVisitDto.builder()
        .date(date)
        .time(LocalTime.of(10, 0))
        .dentistId(0L)
        .build();
  }

  private LocalDate getNextNearestDayOfWeek(LocalDate date, DayOfWeek dayOfWeek) {
    return date.with(TemporalAdjusters.next(dayOfWeek));
  }

  private void mockAvailableTimes(Long dentistId, LocalDate date, LocalTime time) {
    when(mockedDentistVisitService.findAvailableTimes(dentistId, date))
        .thenReturn(time != null ? singletonList(time) : emptyList());
  }

}